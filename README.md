# Status

Branch | Pipeline | Coverage
--- | --- | ---
Prod | [![Pipeline Status](https://gitlab.com/libreorganize/libreorganize/badges/prod/pipeline.svg)](https://gitlab.com/libreorganize/libreorganize/-/commits/prod) | [![Coverage Report](https://gitlab.com/libreorganize/libreorganize/badges/prod/coverage.svg)](https://gitlab.com/libreorganize/libreorganize/-/commits/prod)
Staging | [![Pipeline Status](https://gitlab.com/libreorganize/libreorganize/badges/staging/pipeline.svg)](https://gitlab.com/libreorganize/libreorganize/-/commits/staging) | [![Coverage Report](https://gitlab.com/libreorganize/libreorganize/badges/staging/coverage.svg)](https://gitlab.com/libreorganize/libreorganize/-/commits/staging)
Dev | [![Pipeline Status](https://gitlab.com/libreorganize/libreorganize/badges/dev/pipeline.svg)](https://gitlab.com/libreorganize/libreorganize/-/commits/dev) | [![Coverage Report](https://gitlab.com/libreorganize/libreorganize/badges/dev/coverage.svg)](https://gitlab.com/libreorganize/libreorganize/-/commits/dev)

# About

- LibreOrganize is a web platform for local unions and progressive
  organizations.
- We are helping build popular power through democratic control of our data.

# Installation

1. Clone this repository.

2. Create a virtual environment using `$ python3 -m venv venv`

3. Activate the virtual environment using `$ source venv/bin/activate`.

4. Install the dependencies using `$ pip install -r requirements.txt`.

5. Change the directory to `libreorganize` using `$ cd libreorganize/`.

6. Clone the instance repository using `$ git clone git@gitlab.com:libreorganize/libreorganize-custom-designs instance/`.

7. Apply the migrations using `$ python manage.py migrate`.

8. Load the initial data using `$ python manage.py loaddata instance/fixtures/initial_data.json`.

# How to Use

*Always activate the virtual environment before performing operations.*

- Run the server using `$ python manage.py runserver`.

- Stop the server by pressing `Ctrl-C`.

# Deployment

*Not available yet.*
