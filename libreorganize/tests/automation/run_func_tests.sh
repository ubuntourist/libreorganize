#!/bin/bash

set -e

echo -e "\033[1;31mRunning Functional Tests...\033[0m"
python ../libreorganize/manage.py test tests/func_tests
