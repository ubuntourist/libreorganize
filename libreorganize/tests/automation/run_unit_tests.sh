#!/bin/bash

set -e

echo -e "\033[1;31mRunning Unit Tests with Coverage...\033[0m"
coverage run --source='.' --omit='*/core/*','*/instance/*','../libreorganize/manage.py','*/tests/*','*/migrations/*' ../libreorganize/manage.py test

coverage report -m
coverage erase
