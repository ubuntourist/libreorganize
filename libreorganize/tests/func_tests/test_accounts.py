from django.core import mail
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.contrib.auth.tokens import default_token_generator as token_generator
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from apps.accounts.models import Account
from utils import setup_browser, setup_account, force_login


class AccountsTest(StaticLiveServerTestCase):
    fixtures = ["instance/fixtures/initial_data.json"]

    def setUp(self):
        self.browser = setup_browser()
        self.account = setup_account()

    def tearDown(self):
        self.browser.quit()

    def test_register(self):
        # Heylin needs to manage the membership list of NOVALACIRO.
        # She hears there's a cool platform called LibreOrganize that
        # will do just what she needs. She goes to check out its home page.
        self.browser.get(self.live_server_url + "/")
        assert "Home | LibreOrganize" in self.browser.title

        # She notices links to login and registration.
        self.browser.find_element_by_xpath('//a[@href="/accounts/login/"]').click()

        # Since this is her first time, she chooses to create an account.
        self.browser.find_element_by_xpath('//a[@href="/accounts/register/"]').click()

        # After clicking on the register link, she is presented with a
        # form showing the fields needed for an account.
        assert "Register | LibreOrganize" in self.browser.title
        self.browser.find_element_by_name("email").send_keys("selenium@test.register")
        self.browser.find_element_by_name("verify_email").send_keys("selenium@test.register")
        self.browser.find_element_by_name("password").send_keys("STP12345")
        self.browser.find_element_by_name("verify_password").send_keys("STP12345")
        self.browser.find_element_by_name("first_name").send_keys("Selenium-Test")
        self.browser.find_element_by_name("last_name").send_keys("User")
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight)")
        self.browser.find_element_by_name("terms_of_service").click()
        self.browser.find_element_by_xpath('//button[@type="submit"]').click()

        # After submitting the form, she is redirected to the login page,
        # but this time she sees a message saying she successfully registered.
        assert "Login | LibreOrganize" in self.browser.title
        assert (
            "Welcome to LibreOrganize, Selenium-Test! You have successfully registered."
            in self.browser.find_element_by_class_name("alert-dismissible").text
        )

    def test_login(self):
        self.browser.get(self.live_server_url + "/")

        # Heylin wants to login, so she can use the website. She clicks the
        # login button in the navbar and enters her credentials.
        self.browser.find_element_by_xpath('//a[@href="/accounts/login/"]').click()
        assert "Login | LibreOrganize" in self.browser.title

        self.browser.find_element_by_name("email").send_keys("selenium@test.user")
        self.browser.find_element_by_name("password").send_keys("STP12345")
        self.browser.find_element_by_xpath('//button[@type="submit"]').click()

        # Her username and password are accepted, and she is now logged in to
        # the site.
        assert "Home | LibreOrganize" in self.browser.title
        assert (
            "Welcome back, Selenium-Test! You have successfully logged in."
            in self.browser.find_element_by_class_name("alert-dismissible").text
        )

    def test_logout(self):
        force_login(self.account, self.browser, self.live_server_url)
        self.browser.get(self.live_server_url + "/")

        # Heylin finished working, so she wants to logout. She clicks the
        # logout button and receives a confirmation message after being
        # redirected to the home page.
        self.browser.find_element_by_xpath('//a[@href="/accounts/logout/"]').click()
        assert "Home | LibreOrganize" in self.browser.title
        assert "You have successfully logged out." in self.browser.find_element_by_class_name("alert-dismissible").text

    def test_reset_password(self):
        self.browser.get(self.live_server_url + "/accounts/login")

        # Heylin wants to login, but she forgot her password. She notices the
        # "Forgot your password?" button and clicks it.
        self.browser.find_element_by_xpath('//a[@href="/accounts/password/reset/"]').click()

        # She is redirected to the Reset Password page, enters her email, and
        # submits the form.
        assert "Reset Password | LibreOrganize" in self.browser.title
        self.browser.find_element_by_name("email").send_keys("selenium@test.user")
        self.browser.find_element_by_xpath('//button[@type="submit"]').click()

        # She is redirected to the login page and receives a confirmation
        # message that tells her to check her email.
        assert "Login | LibreOrganize" in self.browser.title
        assert (
            "You have successfully requested a password reset."
            in self.browser.find_element_by_class_name("alert-dismissible").text
        )

        # Heylin sees that she has one unread message from LibreOrganize with
        # the title "Reset Password | LibreOrganize".
        assert len(mail.outbox) == 1
        assert "Reset Password | LibreOrganize" in mail.outbox[0].subject

        # She clicks the "Reset Password" link and she is redirected to a page
        # where she is prompted to enter a new password.
        url = f"{self.live_server_url}/accounts/password/reset/{urlsafe_base64_encode(force_bytes(self.account.uid))}/{token_generator.make_token(self.account)}/"
        assert url in mail.outbox[0].body
        self.browser.get(url)
        assert "Reset Password | LibreOrganize" in self.browser.title

        # Heylin completes the form and submits it.
        self.browser.find_element_by_name("new_password").send_keys("NEW_STP12345")
        self.browser.find_element_by_name("verify_new_password").send_keys("NEW_STP12345")
        self.browser.find_element_by_xpath('//button[@type="submit"]').click()

        # She is redirected to the login page where she sees a message
        # confirming that her password has been reset.
        assert "Login | LibreOrganize" in self.browser.title
        assert (
            "You have successfully reset your password."
            in self.browser.find_element_by_class_name("alert-dismissible").text
        )

    def test_change_password(self):
        force_login(self.account, self.browser, self.live_server_url)
        self.browser.get(self.live_server_url + f"/accounts/{self.account.uid}/")

        # Heylin logins in to change her password to a more secure one. She
        # clicks the "Change Password" button on the left of the page under
        # her profile picture.
        self.browser.find_element_by_xpath(f'//a[@href="/accounts/{self.account.uid}/password/change/"]').click()

        # She is redirected to the "Change Password" page where she completes
        # the form.
        assert "Change Password | LibreOrganize" in self.browser.title
        self.browser.find_element_by_name("new_password").send_keys("NEW_STP12345")
        self.browser.find_element_by_name("verify_new_password").send_keys("NEW_STP12345")
        self.browser.find_element_by_xpath('//button[@type="submit"]').click()

        # She is redirected to the login page where she sees a message
        # confirming that her password has been changed.
        assert "Home | LibreOrganize" in self.browser.title
        assert (
            "You have successfully changed the password."
            in self.browser.find_element_by_class_name("alert-dismissible").text
        )

    def test_list(self):
        force_login(self.account, self.browser, self.live_server_url)
        self.browser.get(self.live_server_url + f"/accounts/")

        # Heylin wants to view a list of registered users. The dashboard took
        # her to the list view page. She can see her account info in the list.
        assert "Accounts | LibreOrganize" in self.browser.title
        assert f"{self.account.uid}" in self.browser.find_element_by_class_name("table").text
        assert "Selenium-Test" in self.browser.find_element_by_class_name("table").text
        assert "selenium@test.user" in self.browser.find_element_by_class_name("table").text
        self.browser.find_element_by_xpath(f'//a[@href="/accounts/{self.account.uid}/"]')
        self.browser.find_element_by_xpath(f'//a[@href="/accounts/{self.account.uid}/edit/?next=/accounts/"]')
        self.browser.find_element_by_xpath(f'//a[@href="/accounts/{self.account.uid}/delete/?next=/accounts/"]')

    def test_detail(self):
        force_login(self.account, self.browser, self.live_server_url)
        self.browser.get(self.live_server_url + f"/accounts/{self.account.uid}/")

        # Heylin wants to view her account's information. She is taken
        # to the detail page from the list view. She notices all the data.
        assert "Selenium-Test User | LibreOrganize" in self.browser.title
        assert f"{self.account.uid}" in self.browser.find_element_by_class_name("table").text
        assert "Selenium-Test" in self.browser.find_element_by_class_name("table").text
        assert "User" in self.browser.find_element_by_class_name("table").text
        assert "selenium@test.user" in self.browser.find_element_by_class_name("table").text

        # She also notices that she is indeed a superuser.
        assert "Superuser Yes" in self.browser.find_element_by_class_name("table").text

    def test_edit(self):
        force_login(self.account, self.browser, self.live_server_url)
        self.browser.get(self.live_server_url + f"/accounts/{self.account.uid}/")

        # Heylin logins in to edit her account information. She clicks
        # the "Edit Account" button on the left of the page under her
        # profile picture.
        self.browser.find_element_by_xpath(
            f'//a[@href="/accounts/{self.account.uid}/edit/?next=/accounts/{self.account.uid}/"]'
        ).click()

        # She is redirected to the "Edit Account" page where she completes
        # the form.
        assert "Edit Account | LibreOrganize" in self.browser.title
        self.browser.find_element_by_name("address1").send_keys("1234 N Arlington Rd")
        self.browser.find_element_by_xpath('//button[@type="submit"]').click()

        # She is redirected to the detail page where she sees a message
        # confirming that her data has been successfully changed.
        # assert "Selenium-Test User | LibreOrganize" in self.browser.title
        assert (
            "The account has been successfully edited."
            in self.browser.find_element_by_class_name("alert-dismissible").text
        )

    def test_delete(self):
        force_login(self.account, self.browser, self.live_server_url)
        self.browser.get(self.live_server_url + f"/accounts/{self.account.uid}/")

        # Heylin wants to delete her account because she doesn't want to use
        # LibreOrgainze anymore. She clicks the "Delete Account" button on
        # the left of the page under her profile picture.
        self.browser.find_element_by_xpath(f'//a[@href="/accounts/{self.account.uid}/delete/"]').click()

        # She is redirected to the "Delete Account" page where she confirms
        # her decision.
        assert "Delete Account | LibreOrganize" in self.browser.title
        self.browser.find_element_by_xpath('//button[@type="submit"]').click()

        # She is redirected to the home page where she sees a message
        # confirming that her account has been deleted.
        assert "Home | LibreOrganize" in self.browser.title
        assert "The account has been deleted." in self.browser.find_element_by_class_name("alert-dismissible").text
