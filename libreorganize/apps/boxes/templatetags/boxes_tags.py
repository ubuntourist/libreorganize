from django import template
from django.urls import reverse
from django.utils.safestring import mark_safe

from apps.boxes.models import Box

register = template.Library()


@register.simple_tag(takes_context=True)
def box(context, uid):
    box = Box.objects.get(uid=uid).content
    request = context["request"]
    if request.user.has_perm("boxes.edit_boxes"):
        box += f'<a class="btn btn-sm btn-warning" href="{reverse("boxes:edit", args={uid})}?next={request.get_full_path()}"><i style="text-shadow: none; color: #000000;" class="fas fa-pen fa-fw"></i></a>'
    return mark_safe(box)
