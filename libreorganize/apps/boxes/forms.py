from django import forms
from django.utils.translation import gettext_lazy as _

from apps.boxes.models import Box


class BoxForm(forms.ModelForm):
    class Meta:
        model = Box
        fields = "__all__"
