from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.shortcuts import render
from django.utils.translation import gettext_lazy as _
from django.views import View

from core.mixins import AccessRestrictedMixin, AccessModelMixin, NextPageMixin
from apps.boxes.models import Box
from apps.boxes.forms import BoxForm


class ListView(AccessRestrictedMixin, View):
    permissions = ("boxes.list_boxes", "boxes.edit_boxes")

    def get(self, request):
        boxes = Box.objects.all()
        return render(request=request, template_name="boxes/list.html", context={"boxes": boxes})


class EditView(AccessRestrictedMixin, AccessModelMixin, NextPageMixin, View):
    permissions = ("boxes.edit_boxes",)
    model = Box

    def get(self, request):
        form = BoxForm(instance=self.box)
        return render(request=request, template_name="boxes/edit.html", context={"form": form})

    def post(self, request):
        form = BoxForm(request.POST, instance=self.box)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, _("The box has been successfully edited."))
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="boxes/edit.html", context={"form": form})
