from django.db import models
from django.utils.translation import gettext_lazy as _

from ckeditor_uploader.fields import RichTextUploadingField


class Box(models.Model):
    uid = models.AutoField(primary_key=True)
    content = RichTextUploadingField()

    class Meta:
        default_permissions = ()
        permissions = (
            ("list_boxes", "List boxes"),
            ("edit_boxes", "Edit boxes"),
        )
