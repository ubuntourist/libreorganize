from apps.boxes import views
from django.urls import path

app_name = "apps.boxes"

urlpatterns = [
    path("", views.ListView.as_view(), name="list"),
    path("<int:uid>/edit/", views.EditView.as_view(), name="edit"),
]
