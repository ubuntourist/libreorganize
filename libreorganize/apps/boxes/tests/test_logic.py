from django.test import TestCase
from apps.accounts.models import Account
from apps.boxes.models import Box


class BoxesListTestCase(TestCase):
    fixtures = [
        "tests/test_data/boxes.json",
        "tests/test_data/accounts.json",
        "instance/fixtures/initial_data.json",
    ]

    def setUp(self):
        """Set up accounts"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.box = Box.objects.get(content="Box Content")

    def test_template(self):
        """Check list template"""
        self.client.force_login(self.superuser)
        response = self.client.get("/boxes/", follow=True)
        self.assertTemplateUsed(response, template_name="boxes/list.html")

    def test_successful(self):
        """Check list successful"""
        self.client.force_login(self.superuser)
        response = self.client.get("/boxes/", follow=True)
        self.assertContains(response, self.box.uid)
        self.assertContains(response, self.box.content)

    def test_incorrect_permissions(self):
        """Check list unsuccessful (permissions)"""
        self.client.force_login(self.user)
        response = self.client.get("/boxes/", follow=True)
        self.assertContains(response, "You don&#39;t have the required permissions.")


class BoxesEditTestCase(TestCase):
    fixtures = [
        "tests/test_data/boxes.json",
        "tests/test_data/accounts.json",
        "instance/fixtures/initial_data.json",
    ]

    def setUp(self):
        """Set up accounts"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.box = Box.objects.get(content="Box Content")

    def test_template(self):
        """Check edit template"""
        self.client.force_login(self.superuser)
        response = self.client.get(f"/boxes/{self.box.uid}/edit/", follow=True)
        self.assertTemplateUsed(response, template_name="boxes/edit.html")

    def test_successful(self):
        """Check edit successful"""
        self.client.force_login(self.superuser)
        response = self.client.post(f"/boxes/{self.box.uid}/edit/", {"content": "Changed Content"}, follow=True)
        self.assertContains(response, "The box has been successfully edited.")

    def test_incorrect_content(self):
        """Check edit unsuccessful (content)"""
        self.client.force_login(self.superuser)
        response = self.client.post(f"/boxes/{self.box.uid}/edit/", {"content": ""}, follow=True)
        self.assertContains(response, "This field is required.")

    def test_incorrect_box(self):
        """Check edit unsuccessful (box)"""
        self.client.force_login(self.superuser)
        response = self.client.get("/boxes/999999/edit/", follow=True)
        self.assertContains(response, "The box doesn&#39;t exist.")

    def test_incorrect_permissions(self):
        """Check edit unsuccessful (permissions)"""
        self.client.force_login(self.user)
        response = self.client.get(f"/boxes/{self.box.uid}/edit/", follow=True)
        self.assertContains(response, "You don&#39;t have the required permissions.")
