from django.test import TestCase
from django.urls import resolve


class BoxesUrlsCase(TestCase):
    def test_list(self):
        """Check list URL correct"""
        self.assertEqual(resolve("/boxes/").view_name, "boxes:list")

    def test_edit(self):
        """Check edit URL correct"""
        self.assertEqual(resolve("/boxes/999999/edit/").view_name, "boxes:edit")
