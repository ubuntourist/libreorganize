from django.test import TestCase
from apps.boxes.apps import BoxesConfig


class BoxesAppsTestCase(TestCase):
    def test_name(self):
        """Check name correct"""
        self.assertEqual(BoxesConfig.name, "apps.boxes")
