from apps.memberships import views
from django.urls import path

app_name = "apps.memberships"

urlpatterns = [
    path("", views.ListView.as_view(), name="list"),
    path("create/", views.CreateView.as_view(), name="create"),
    path("enroll/", views.EnrollView.as_view(), name="enroll"),
    path("<int:uid>/", views.DetailView.as_view(), name="detail"),
    path("<int:uid>/edit/", views.EditView.as_view(), name="edit"),
    path("<int:uid>/delete/", views.DeleteView.as_view(), name="delete"),
]
