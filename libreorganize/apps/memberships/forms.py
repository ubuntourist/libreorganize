from django import forms
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from tempus_dominus.widgets import DatePicker

from apps.memberships.models import Membership


class EnrollForm(forms.ModelForm):
    class Meta:
        model = Membership
        fields = ["kind", "payment_method"]
        labels = {"kind": "Type", "payment_method": "Payment Method"}


class CreateForm(forms.ModelForm):
    class Meta:
        model = Membership
        fields = ["kind", "start_date", "member"]
        labels = {"kind": "Type", "start_date": "Start Date", "member": "Member"}
        widgets = {
            "start_date": DatePicker(
                options={
                    "format": "YYYY-MM-DD",
                    "icons": {"up": "fas fa-chevron-up fa-fw", "down": "fas fa-chevron-down fa-fw",},
                },
                attrs={"append": "fas fa-calendar-alt fa-fw"},
            ),
        }


class EditForm(forms.ModelForm):
    class Meta:
        model = Membership
        fields = ["kind", "start_date", "payment_method", "member", "is_active"]
        labels = {
            "kind": "Type",
            "start_date": "Start Date",
            "payment_method": "Payment Method",
            "member": "Member",
            "is_active": "Active",
        }
        widgets = {
            "start_date": DatePicker(
                options={
                    "format": "YYYY-MM-DD",
                    "icons": {"up": "fas fa-chevron-up fa-fw", "down": "fas fa-chevron-down fa-fw"},
                },
                attrs={"append": "fas fa-calendar-alt fa-fw"},
            ),
        }

    def __init__(self, *args, **kwargs):
        super(EditForm, self).__init__(*args, **kwargs)
        self.fields["member"].disabled = True

