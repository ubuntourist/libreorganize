from django.test import TestCase
from apps.accounts.models import Account
from apps.memberships.models import Membership


class TestMembershipsList(TestCase):
    fixtures = [
        "instance/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/memberships.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.membership = Membership.objects.get(member=self.superuser.uid)

    def test_template(self):
        """Check templates"""
        self.client.force_login(self.superuser)
        response = self.client.get("/memberships/")
        self.assertTemplateUsed(response, template_name="memberships/list.html")

    def test_successful(self):
        """Test Successful Membership"""
        self.client.force_login(self.superuser)
        response = self.client.get("/memberships/")
        self.assertContains(response, "2")

    def test_invalid_permissions(self):
        """Invalid Perms"""
        self.client.force_login(self.user)
        response = self.client.get("/memberships/", follow=True)
        self.assertContains(response, "You don&#39;t have the required permissions.")
        

class TestMembershipsCreate(TestCase):
    fixtures = [
        "instance/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/memberships.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.membership = Membership.objects.get(member=self.superuser.uid)

    def test_template(self):
        """Check templates"""
        self.client.force_login(self.superuser)
        response = self.client.get("/memberships/create/")
        self.assertTemplateUsed(response, template_name="memberships/create.html")
        response = self.client.post("/memberships/create/")
        self.assertTemplateUsed(response, template_name="memberships/create.html")
    
    def test_successful(self):
        """Test successful create Membership"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/memberships/create/",
            {
                "kind": "student",
                "start_date": "2020-04-01",
                "member": "1",
            },
            follow=True,
        )
        self.assertContains(response, "The membership has been successfully created.")
    
    def test_invalid_permissions(self):
        """Invalid Perms"""
        self.client.force_login(self.user)
        response = self.client.get("/memberships/create/", follow=True)
        self.assertContains(response, "You don&#39;t have the required permissions.")

class TestMembershipsEnroll(TestCase):
    fixtures = [
        "instance/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/memberships.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.membership = Membership.objects.get(member=self.superuser.uid)

    def test_template(self):
        """Check templates"""
        self.client.force_login(self.user)
        response = self.client.get("/memberships/enroll/")
        self.assertTemplateUsed(response, template_name="memberships/enroll.html")
        response = self.client.post("/memberships/enroll/")
        self.assertTemplateUsed(response, template_name="memberships/enroll.html")

    def test_already_member(self):
        """Already a memeber"""
        self.client.force_login(self.superuser)
        response = self.client.get("/memberships/enroll/", follow=True)
        self.assertContains(response, "You are already a member!")
        response = self.client.post("/memberships/enroll/", follow=True)
        self.assertContains(response, "You are already a member!")

    def test_use_cash(self):
        """Use Cash to Pay for Memebership"""
        self.client.force_login(self.user)
        response = self.client.post(
            "/memberships/enroll/",
            {
                "kind": "student",
                "payment_method": "cash",
            },
            follow=True,
        )
        self.assertContains(response, "Congratulations, Test! You are almost a member of LibreOrganize. An administrator will activate your membership soon.")
       


class TestMembershipsDetail(TestCase):
    fixtures = [
        "instance/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/memberships.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.membership = Membership.objects.get(member=self.superuser.uid)

    def test_template(self):
        """Check templates"""
        self.client.force_login(self.superuser)
        response = self.client.get("/memberships/2/")
        self.assertTemplateUsed(response, template_name="memberships/detail.html")
        
        
class TestMembershipsEdit(TestCase):
    fixtures = [
        "instance/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/memberships.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.membership = Membership.objects.get(member=self.superuser.uid)

    def test_template(self):
        """Check templates"""
        self.client.force_login(self.superuser)
        response = self.client.get("/memberships/2/edit/")
        self.assertTemplateUsed(response, template_name="memberships/edit.html")
        response = self.client.post("/memberships/2/edit/")
        self.assertTemplateUsed(response, template_name="memberships/edit.html")

    def test_edit(self):
        """Test Edit"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/memberships/2/edit/",
            {
                "kind": "student",
                "start_date": "2020-04-01",
                "payment_method": "cash",
                "is_active": True,
            },
            follow=True,
        )
        self.assertContains(response, "The membership has been successfully edited.")
    
    def test_invalid_permissions(self):
        """Invalid Permissions"""
        self.client.force_login(self.user)
        response = self.client.post("/memberships/2/edit/", follow=True)
        self.assertContains(response, "You don&#39;t have the required permissions.")

class TestMembershipsDelete(TestCase):
    fixtures = [
        "instance/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/memberships.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.membership = Membership.objects.get(member=self.superuser.uid)

    def test_template(self):
        """Check templates"""
        self.client.force_login(self.superuser)
        response = self.client.get("/memberships/2/delete/")
        self.assertTemplateUsed(response, template_name="memberships/delete.html")

    def test_delete(self):
        """Delete Membership"""
        self.client.force_login(self.superuser)
        response = self.client.post("/memberships/2/delete/", follow=True)
        self.assertContains(response, "The membership has been deleted.")
        
