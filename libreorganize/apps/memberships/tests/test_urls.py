from django.test import TestCase
from django.urls import resolve

class MembershipsUrlsCase(TestCase):
    def test_list(self):
        """Check list URL correct"""
        self.assertEqual(resolve("/memberships/").view_name, "memberships:list")

    def test_create(self):
        """Check create URL correct"""
        self.assertEqual(resolve("/memberships/create/").view_name, "memberships:create")

    def test_enroll(self):
        """Check enroll correct"""
        self.assertEqual(resolve("/memberships/enroll/").view_name, "memberships:enroll")

    def test_detail(self):
        """Check detail URL correct"""
        self.assertEqual(resolve("/memberships/1/").view_name, "memberships:detail")

    def test_edit(self):
        """Check edit URL correct"""
        self.assertEqual(resolve("/memberships/1/edit/").view_name, "memberships:edit")

    def test_delete(self):
        """Check delete URL correct"""
        self.assertEqual(resolve("/memberships/1/delete/").view_name, "memberships:delete")


