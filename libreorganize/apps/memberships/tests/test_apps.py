from django.test import TestCase
from apps.memberships.apps import MembershipsConfig


class MembershipsAppsTestCase(TestCase):
    def test_name(self):
        """Check name correct"""
        self.assertEqual(MembershipsConfig.name, "apps.memberships")
