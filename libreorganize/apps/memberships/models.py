import datetime

from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

from apps.accounts.models import Account


class Membership(models.Model):
    uid = models.AutoField(primary_key=True)
    kind = models.CharField(max_length=16, choices=settings.MEMBERSHIP_TYPES, default="normal")
    start_date = models.DateField(help_text=_("Must be formatted as YYYY-MM-DD"))
    payment_method = models.CharField(max_length=8, choices=settings.PAYMENT_METHODS, default="cash")
    member = models.OneToOneField(
        Account,
        on_delete=models.CASCADE,
        error_messages={"unique": _("There is already a membership associated with this account.")},
    )
    is_active = models.BooleanField(default=False)

    class Meta:
        default_permissions = ()
        permissions = (
            ("list_memberships", "List memberships"),
            ("create_memberships", "Create memberships"),
            ("edit_memberships", "Edit memberships"),
            ("delete_memberships", "Delete memberships"),
        )
