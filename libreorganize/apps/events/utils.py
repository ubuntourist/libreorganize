import datetime
import calendar

from django.urls import reverse
from django.utils.safestring import mark_safe

from apps.events.models import Event


class Calendar(calendar.HTMLCalendar):
    def __init__(self, year=None, month=None):
        self.year = year
        self.month = month
        super().__init__()

    # formats a day as a td
    # filter events by day
    def formatday(self, day, events):
        events_per_day = events.filter(start_date__day=day).order_by("start_date")
        d = ""
        for event in events_per_day:
            d += f"<a class='calendar-event' href='{reverse('events:detail', args={event.uid})}'>{event.title}</a> <a class='calendar-event-mobile' href='{reverse('events:detail', args={event.uid})}'><i class='fas fa-dot-circle'></i></a>"

        if day != 0:
            return f"<td><span class='date'>{day}</span><div> {d} </div></td>"
        return "<td></td>"

    # formats a week as a tr
    def formatweek(self, theweek, events):
        week = ""
        for d, weekday in theweek:
            week += self.formatday(d, events)
        return f"<tr> {week} </tr>"

    # formats a month as a table
    # filter events by year and month
    def formatmonth(self, withyear=True):
        events = Event.objects.filter(start_date__year=self.year, start_date__month=self.month)

        cal = (
            f'<table border="0" cellpadding="0" cellspacing="0" class="table table-striped table-bordered calendar">\n'
        )
        cal += f"{self.formatmonthname(self.year, self.month, withyear=withyear)}\n"
        cal += f"{self.formatweekheader()}\n"
        for week in self.monthdays2calendar(self.year, self.month):
            cal += f"{self.formatweek(week, events)}\n"
        cal += "</table>"
        return mark_safe(cal)


def get_date(req_day):
    if req_day:
        year, month = (int(x) for x in req_day.split("-"))
        return datetime.date(year, month, day=1)
    return datetime.datetime.today()


def prev_month(d):
    first = d.replace(day=1)
    prev_month = first - datetime.timedelta(days=1)
    month = "month=" + str(prev_month.year) + "-" + str(prev_month.month)
    return month


def next_month(d):
    days_in_month = calendar.monthrange(d.year, d.month)[1]
    last = d.replace(day=days_in_month)
    next_month = last + datetime.timedelta(days=1)
    month = "month=" + str(next_month.year) + "-" + str(next_month.month)
    return month
