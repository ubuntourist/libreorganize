from django.db import models
from django.utils.translation import gettext_lazy as _

from apps.accounts.models import Account


class Event(models.Model):
    uid = models.AutoField(primary_key=True)
    title = models.CharField(max_length=32)
    description = models.TextField(blank=True)
    start_date = models.DateTimeField(help_text=_("Must be formatted as YYYY-MM-DD HH:MM"))
    end_date = models.DateTimeField(help_text=_("Must be formatted as YYYY-MM-DD HH:MM"))
    location = models.CharField(max_length=128, help_text=_("Should be an address"))
    maximum_no_participants = models.IntegerField(
        default=-1, help_text="Enter -1 for an unlimited number of participants"
    )
    participants = models.ManyToManyField(Account, blank=True)

    class Meta:
        default_permissions = ()
        permissions = (
            ("list_events", "List events"),
            ("create_events", "Create events"),
            ("edit_events", "Edit events"),
            ("delete_events", "Delete events"),
        )
