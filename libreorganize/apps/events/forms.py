from django import forms
from django.utils.translation import gettext_lazy as _

from tempus_dominus.widgets import DateTimePicker

from apps.events.models import Event


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        exclude = ("participants",)
        widgets = {
            "start_date": DateTimePicker(
                options={
                    "format": "YYYY-MM-DD HH:mm",
                    "icons": {
                        "time": "fas fa-clock fa-fw",
                        "date": "fas fa-calendar-alt fa-fw",
                        "up": "fas fa-chevron-up fa-fw",
                        "down": "fas fa-chevron-down fa-fw",
                    },
                },
                attrs={"append": "fas fa-calendar-alt fa-fw"},
            ),
            "end_date": DateTimePicker(
                options={
                    "format": "YYYY-MM-DD HH:mm",
                    "icons": {
                        "time": "fas fa-clock fa-fw",
                        "date": "fas fa-calendar-alt fa-fw",
                        "up": "fas fa-chevron-up fa-fw",
                        "down": "fas fa-chevron-down fa-fw",
                    },
                },
                attrs={"append": "fas fa-calendar-alt fa-fw"},
            ),
        }
        labels = {
            "start_date": "Start Date",
            "end_date": "End Date",
        }

    def clean(self):
        self.cleaned_data = super().clean()
        if self._errors:
            return
        if self.cleaned_data["end_date"] < self.cleaned_data["start_date"]:
            raise forms.ValidationError(_("The end date cannot be less than the start date."))
        return self.cleaned_data

    def clean_maximum_no_participants(self):
        if self.cleaned_data["maximum_no_participants"] < -1:
            raise forms.ValidationError(_("The maximum number of participants needs to be higher than -1."))
        return self.cleaned_data["maximum_no_participants"]

