from django.views import View
from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.utils.translation import gettext_lazy as _

from core.mixins import AccessRestrictedMixin, AccessModelMixin, NextPageMixin
from apps.events.models import Event
from apps.events.forms import EventForm
from apps.events.utils import Calendar, get_date, prev_month, next_month


class CalendarView(View):
    def get(self, request, *args, **kwargs):
        date = get_date(self.request.GET.get("month", None))
        calendar = Calendar(date.year, date.month)

        context = {
            "calendar": calendar.formatmonth(withyear=True),
            "prev_month": prev_month(date),
            "next_month": next_month(date),
        }

        return render(request=request, template_name="events/calendar.html", context=context)


class ListView(AccessRestrictedMixin, View):
    permissions = ("events.list_events",)

    def get(self, request):
        events = Event.objects.all().order_by("start_date")
        return render(request=request, template_name="events/list.html", context={"events": events})


class CreateView(AccessRestrictedMixin, NextPageMixin, View):
    permissions = ("events.create_events",)

    def get(self, request):
        form = EventForm()
        return render(request=request, template_name="events/create.html", context={"form": form})

    def post(self, request):
        form = EventForm(request.POST)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, _("The event has been successfully created."))
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="events/create.html", context={"form": form})


class DetailView(AccessModelMixin, View):
    model = Event

    def get(self, request):
        return render(request=request, template_name="events/detail.html", context={"event": self.event})


class EditView(AccessRestrictedMixin, AccessModelMixin, NextPageMixin, View):
    permissions = ("events.edit_events",)
    model = Event

    def get(self, request):
        form = EventForm(instance=self.event)
        return render(request=request, template_name="events/edit.html", context={"form": form})

    def post(self, request):
        form = EventForm(request.POST, request.FILES, instance=self.event)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, _("The event has been successfully edited."))
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="events/edit.html", context={"form": form})


class DeleteView(AccessRestrictedMixin, AccessModelMixin, NextPageMixin, View):
    permissions = ("events.delete_events",)
    model = Event

    def get(self, request):
        return render(request=request, template_name="events/delete.html")

    def post(self, request):
        self.event.delete()
        messages.add_message(request, messages.SUCCESS, _("The event has been deleted."))
        return HttpResponseRedirect(self.next)


class CheckView(AccessRestrictedMixin, AccessModelMixin, View):
    model = Event

    def get(self, request):
        if request.user in self.event.participants.all():
            messages.add_message(request, messages.WARNING, _("You have already checked in!"))
        else:
            if (
                self.event.participants.count() >= self.event.maximum_no_participants
                and self.event.maximum_no_participants != -1
            ):
                messages.add_message(request, messages.ERROR, _("There are no more empty seats for this event!"))
            else:
                self.event.participants.add(request.user)
                messages.add_message(request, messages.SUCCESS, _("You checked in."))
        return HttpResponseRedirect(reverse("events:detail", args={self.event.uid}))
