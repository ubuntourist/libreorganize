from django.test import TestCase
from apps.accounts.models import Account
from apps.events.models import Event


class TestEventViews(TestCase):
    fixtures = [
        "instance/fixtures/initial_data.json",
        "tests/test_data/accounts.json",
        "tests/test_data/events.json",
    ]

    def setUp(self):
        """Set up accounts and events"""
        self.user = Account.objects.get(is_superuser=False)
        self.superuser = Account.objects.get(is_superuser=True)
        self.event1 = Event.objects.get(uid=1)
        self.event2 = Event.objects.get(uid=2)

    def test_template(self):
        """Check templates"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/")
        self.assertTemplateUsed(response, template_name="events/list.html")
        response = self.client.get("/events/calendar/")
        self.assertTemplateUsed(response, template_name="events/calendar.html")
        response = self.client.get("/events/create/")
        self.assertTemplateUsed(response, template_name="events/create.html")
        response = self.client.get("/events/1/edit/")
        self.assertTemplateUsed(response, template_name="events/edit.html")
        response = self.client.get("/events/1/delete/")
        self.assertTemplateUsed(response, template_name="events/delete.html")

    def test_incorrect_permissions_edit(self):
        """Incorrect Permissions to Edit"""
        self.client.force_login(self.user)
        response = self.client.get("/events/1/edit/", follow=True)
        self.assertContains(response, "You don&#39;t have the required permissions.")
        self.assertRedirects(response, "/")

    def test_incorrect_permissions_delete(self):
        """Incorrect Permissions to Delete"""
        self.client.force_login(self.user)
        response = self.client.get("/events/1/delete/", follow=True)
        self.assertContains(response, "You don&#39;t have the required permissions.")
        self.assertRedirects(response, "/")

    def test_event_does_not_exist_edit(self):
        """Check if Event Exists Edit"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/5/edit/", follow=True)
        self.assertContains(response, "The event doesn&#39;t exist.")
        self.assertRedirects(response, "/")

    def test_event_does_not_exist(self):
        """Check if Event Exists"""
        response = self.client.get("/events/100/", follow=True)
        self.assertContains(response, "The event doesn&#39;t exist.")
        self.assertRedirects(response, "/")

    def test_event_does_not_exist_delete(self):
        """Check if Event Exists Delete"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/5/delete/", follow=True)
        self.assertContains(response, "The event doesn&#39;t exist.")
        self.assertRedirects(response, "/")

    def test_event_does_not_exist_check(self):
        """Check if Event Exists Check"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/5/check/", follow=True)
        self.assertContains(response, "The event doesn&#39;t exist.")
        self.assertRedirects(response, "/")

    def test_event_does_exist(self):
        """Event Exists bring to details"""
        self.client.force_login(self.superuser)
        response = self.client.get("/events/1/edit/", follow=True)
        self.assertContains(response, "Start Date")

    def test_event_detail_page(self):
        """Bring to Detail Page"""
        response = self.client.get("/events/1", follow=True)
        self.assertContains(response, "Start Date")

    def test_check_in(self):
        """Check in & out"""
        self.client.force_login(self.user)
        response = self.client.get("/events/1/check/", follow=True)
        self.assertRedirects(response, "/events/1/")
        self.assertContains(response, "You checked in.")
        response = self.client.get("/events/1/check/", follow=True)
        self.assertRedirects(response, "/events/1/")
        self.assertContains(response, "You have already checked in!")

    def test_delete(self):
        """Check delete"""
        self.client.force_login(self.superuser)
        response = self.client.post("/events/1/delete/", follow=True)
        self.assertContains(response, "The event has been deleted.")

    def test_max_participant(self):
        """No more participants allowed"""
        self.client.force_login(self.user)
        response = self.client.get("/events/2/check/", follow=True)
        self.assertRedirects(response, "/events/2/")
        self.assertContains(response, "You checked in.")
        self.client.force_login(self.superuser)
        response = self.client.get("/events/2/check/", follow=True)
        self.assertContains(response, "There are no more empty seats for this event!")

    def test_end_date_before(self):
        """Check register invalid (date)"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-02-01 12:45",
                "location": "1234 Washington Blvd",
                "maximum_no_participants": "-1",
            },
            follow=True,
        )
        self.assertContains(response, "The end date cannot be less than the start date.")

    def test_no_date(self):
        """No Date"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-03-01 13:45",
                "location": "1234 Washington Blvd",
                "maximum_no_participants": "-1",
            },
            follow=True,
        )
        self.assertContains(response, "")

    def test_max_part_less_than_neg_one(self):
        """Maximum Participant Less than -1"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-03-01 13:45",
                "location": "1234 Washington Blvd",
                "maximum_no_participants": "-2",
            },
            follow=True,
        )
        self.assertContains(response, "The maximum number of participants needs to be higher than -1.")

    def test_edit_event(self):
        """edit event"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/1/edit/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 14:45",
                "end_date": "2020-03-01 13:45",
                "location": "1234 Washington Blvd",
                "maximum_no_participants": "-1",
            },
            follow=True,
        )
        self.assertContains(response, "The end date cannot be less than the start date.")
        response = self.client.post(
            "/events/1/edit/",
            {
                "title": "Test2",
                "description": "New Desc",
                "start_date": "2020-04-01 11:45",
                "end_date": "2020-04-01 13:45",
                "location": "1234 Washington Blvd",
                "maximum_no_participants": "-1",
            },
            follow=True,
        )
        self.assertContains(response, "The event has been successfully edited.")
        self.assertRedirects(response, "/")

    def test_months(self):
        """Check to make sure user can change months"""
        response = self.client.get("/events/calendar/?month=2020-4")
        self.assertContains(response, "April 2020")
        response = self.client.get("/events/calendar/?month=2020-5")
        self.assertContains(response, "May 2020")

    def test_successful_event(self):
        """Check successful event"""
        self.client.force_login(self.superuser)
        response = self.client.post(
            "/events/create/",
            {
                "title": "Test",
                "description": "This is a test",
                "start_date": "2020-03-01 11:45",
                "end_date": "2020-03-01 12:45",
                "location": "1234 Washington Blvd",
                "maximum_no_participants": "-1",
            },
            follow=True,
        )
        response = self.client.get("/events/calendar/?month=2020-3")
        self.assertContains(response, "Test")

