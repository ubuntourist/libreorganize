from django.test import TestCase
from django.urls import resolve


class EventsUrlsCase(TestCase):
    def test_list(self):
        """Check list URL correct"""
        self.assertEqual(resolve("/events/").view_name, "events:list")

    def test_calendar(self):
        """Check calendar URL correct"""
        self.assertEqual(resolve("/events/calendar/").view_name, "events:calendar")

    def test_create(self):
        """Check create URL correct"""
        self.assertEqual(resolve("/events/create/").view_name, "events:create")

    def test_detail(self):
        """Check detail correct"""
        self.assertEqual(resolve("/events/999999/").view_name, "events:detail")

    def test_edit(self):
        """Check edit URL correct"""
        self.assertEqual(resolve("/events/999999/edit/").view_name, "events:edit")

    def test_delete(self):
        """Check delete URL correct"""
        self.assertEqual(resolve("/events/999999/delete/").view_name, "events:delete")

    def test_check(self):
        """Check check correct"""
        self.assertEqual(resolve("/events/999999/check/").view_name, "events:check")

