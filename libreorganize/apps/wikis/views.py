from django.shortcuts import get_object_or_404
from django.utils.safestring import mark_safe
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib import messages
from django.utils.translation import gettext_lazy as _
from django.views import View

from core.mixins import AccessRestrictedMixin, AccessModelMixin, NextPageMixin

from apps.accounts.models import Account
from apps.wikis.models import Wiki, Log
from apps.wikis.forms import WikiForm


class CreateView(AccessRestrictedMixin, NextPageMixin, View):
    permissions = ("wikis.create_wikis",)

    def get(self, request):
        form = WikiForm()
        return render(request=request, template_name="wikis/create.html", context={"form": form})

    def post(self, request):
        form = WikiForm(request.POST)
        if form.is_valid():
            wiki = form.save()
            Log(author=request.user, wiki=wiki, content=form.cleaned_data["content"], action="Create").save()
            messages.add_message(request, messages.SUCCESS, _("The wiki has been successfully created."))
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="wikis/create.html", context={"form": form})


class EditView(AccessRestrictedMixin, NextPageMixin, View):
    permissions = ("wikis.edit_wikis",)

    def dispatch(self, request, url, *args, **kwargs):
        try:
            self.wiki = Wiki.objects.get(url=url.lower())
        except (TypeError, ValueError, OverflowError, Wiki.DoesNotExist):
            messages.add_message(request, messages.ERROR, _("The wiki doesn't exist."))
            return HttpResponseRedirect("/")
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        form = WikiForm(instance=self.wiki)
        return render(request=request, template_name="wikis/edit.html", context={"form": form})

    def post(self, request):
        form = WikiForm(request.POST, instance=self.wiki)
        if form.is_valid():
            form.save()
            Log(author=request.user, wiki=self.wiki, content=form.cleaned_data["content"], action="Edit").save()
            messages.add_message(request, messages.SUCCESS, _("The wiki has been successfully edited."))
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="wikis/edit.html", context={"form": form})


class DeleteView(AccessRestrictedMixin, NextPageMixin, View):
    permissions = ("wikis.delete_wikis",)

    def dispatch(self, request, url, *args, **kwargs):
        if url.lower() == "home":
            messages.add_message(request, messages.ERROR, _("The wiki cannot be deleted."))
            return HttpResponseRedirect("/")
        try:
            self.wiki = Wiki.objects.get(url=url.lower())
        except (TypeError, ValueError, OverflowError, Wiki.DoesNotExist):
            messages.add_message(request, messages.ERROR, _("The wiki doesn't exist."))
            return HttpResponseRedirect("/")
        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        return render(request=request, template_name="wikis/delete.html")

    def post(self, request):
        self.wiki.delete()
        Log(author=request.user, wiki=None, content="", action="Delete").save()
        messages.add_message(request, messages.SUCCESS, _("The wiki has been deleted."))
        return HttpResponseRedirect(self.next)


class WikiView(AccessRestrictedMixin, View):
    permissions = ("wikis.view_wikis",)

    def dispatch(self, request, url, *args, **kwargs):
        try:
            self.wiki = Wiki.objects.get(url=url.lower())
        except (TypeError, ValueError, OverflowError, Wiki.DoesNotExist):
            messages.add_message(request, messages.ERROR, _("The wiki doesn't exist."))
            return HttpResponseRedirect("/")
        self.wikis = Wiki.objects.all()
        for wiki in self.wikis:
            if "[[" + wiki.title + "]]" in self.wiki.content:
                self.wiki.content = self.wiki.content.replace(
                    "[[" + wiki.title + "]]", f"<a href=/wikis/{wiki.url}/>{wiki.title}</a>"
                )
        self.wiki.content = mark_safe(self.wiki.content)

        return super().dispatch(request, *args, **kwargs)

    def get(self, request):
        self.wikis = list(self.wikis.order_by("title"))
        for wiki in self.wikis:
            if wiki.url == "home":
                self.wikis.insert(0, self.wikis.pop(self.wikis.index(wiki)))
                break
        return render(
            request=request, template_name="wikis/base.html", context={"wiki": self.wiki, "wikis": self.wikis}
        )

    def post(self, request):
        self.wikis = list(Wiki.objects.filter(title__icontains=request.POST["search"]).order_by("title"))
        if request.POST["search"] == "":
            for wiki in self.wikis:
                if wiki.url == "home":
                    self.wikis.insert(0, self.wikis.pop(self.wikis.index(wiki)))
                    break
        return render(
            request=request, template_name="wikis/base.html", context={"wiki": self.wiki, "wikis": self.wikis}
        )


class LogsListView(AccessRestrictedMixin, View):
    permissions = ("wikis.view_wikis",)

    def get(self, request):
        if request.GET.get("search", None):
            logs = Log.objects.filter(wiki__url=request.GET.get("search", "")).order_by("-date")
        else:
            logs = Log.objects.all().order_by("-date")
        return render(request=request, template_name="wikis/logs/list.html", context={"logs": logs})


class LogsDetailView(AccessRestrictedMixin, AccessModelMixin, View):
    permissions = ("wikis.view_wikis",)
    model = Log

    def get(self, request):
        self.log.content = mark_safe(self.log.content)
        return render(request=request, template_name="wikis/logs/detail.html", context={"log": self.log})
