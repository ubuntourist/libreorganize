import re

from django import forms
from django.utils.translation import gettext_lazy as _

from apps.wikis.models import Wiki


class WikiForm(forms.ModelForm):
    class Meta:
        model = Wiki
        fields = ("title", "content")

    def clean_title(self):
        try:
            wiki = Wiki.objects.get(url=self.cleaned_data["title"].replace(" ", "-").lower())
            if wiki == self.instance:
                raise Wiki.DoesNotExist
            raise forms.ValidationError(_("A wiki with a similar title already exists."))
        except Wiki.DoesNotExist:
            if not re.match("^[a-zA-Z0-9\$\-\_\.\+\'\(\)\=\ ]*$", self.cleaned_data["title"]):
                raise forms.ValidationError(_("The title must contain only URL-safe characters or spaces."))
            if self.cleaned_data["title"].lower() == "create":
                raise forms.ValidationError(_('"Create" is not a valid title.'))
            if self.cleaned_data["title"].lower() == "logs":
                raise forms.ValidationError(_('"Logs" is not a valid title.'))
            if len(self.cleaned_data["title"]) < 3:
                raise forms.ValidationError(_("The title must have at least 3 characters."))
            return self.cleaned_data["title"]
