from django.conf import settings
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.shortcuts import render
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import Permission
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.tokens import default_token_generator as token_generator
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.views import View

from core.mixins import AccessRestrictedMixin, AccessModelMixin, NextPageMixin
from apps.accounts.models import Account
from apps.accounts.forms import *


class LoginView(View):
    def dispatch(self, request):
        self.next = request.GET.get("next", None)
        if not self.next or "://" in self.next or " " in self.next:
            self.next = "/"
        if request.user.is_authenticated:
            messages.add_message(request, messages.WARNING, _("You are already logged in!"))
            return HttpResponseRedirect(self.next)
        return super().dispatch(request)

    def get(self, request):
        form = LoginForm()
        return render(request=request, template_name="accounts/login.html", context={"form": form})

    def post(self, request):
        form = LoginForm(request.POST)
        if form.login(request):
            messages.add_message(
                request,
                messages.SUCCESS,
                _(f"Welcome back, {request.user.first_name}! You have successfully logged in."),
            )
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="accounts/login.html", context={"form": form})


class LogoutView(View):
    def get(self, request):
        if request.user.is_authenticated:
            logout(request)
            messages.add_message(request, messages.SUCCESS, _("You have successfully logged out."))
        else:
            messages.add_message(request, messages.WARNING, _("You are already logged out!"))
        return HttpResponseRedirect(reverse("home"))


class RegisterView(View):
    def dispatch(self, request):
        if request.user.is_authenticated:
            messages.add_message(request, messages.WARNING, _("You cannot register an account while being logged in!"))
            return HttpResponseRedirect(reverse("home"))
        return super().dispatch(request)

    def get(self, request):
        form = RegistrationForm()
        return render(request=request, template_name="accounts/register.html", context={"form": form})

    def post(self, request):
        form = RegistrationForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.add_message(
                request,
                messages.SUCCESS,
                _(
                    f"Welcome to {settings.SITE_NAME}, {form.cleaned_data['first_name']}! You have successfully registered."
                ),
            )
            return HttpResponseRedirect(reverse("accounts:login"))
        return render(request=request, template_name="accounts/register.html", context={"form": form})


class PasswordResetView(View):
    def dispatch(self, request):
        if request.user.is_authenticated:
            messages.add_message(
                request,
                messages.WARNING,
                _("You have been redirected to change your password because you are logged in!"),
            )
            return HttpResponseRedirect(reverse("accounts:password_change", args={request.user.uid}))
        return super().dispatch(request)

    def get(self, request):
        form = PasswordResetForm()
        return render(request=request, template_name="accounts/password/reset.html", context={"form": form})

    def post(self, request):
        form = PasswordResetForm(request.POST)
        if form.is_valid():
            form.save(request=request)
            messages.add_message(
                request, messages.SUCCESS, _("You have successfully requested a password reset."),
            )
            return HttpResponseRedirect(reverse("accounts:login"))
        return render(request=request, template_name="accounts/password/reset.html", context={"form": form})


class PasswordResetConfirmView(View):
    def dispatch(self, request, uidb64, token):
        if request.user.is_authenticated:
            messages.add_message(
                request,
                messages.WARNING,
                _("You have been redirected to change your password because you are logged in!"),
            )
            return HttpResponseRedirect(reverse("accounts:password_change", args={request.user.uid}))
        try:
            self.account = Account.objects.get(uid=force_text(urlsafe_base64_decode(uidb64)))
            if token_generator.check_token(self.account, token):
                return super().dispatch(request)
            raise Account.DoesNotExist
        except (TypeError, ValueError, OverflowError, Account.DoesNotExist):
            messages.add_message(request, messages.ERROR, _("The request is invalid."))
            return HttpResponseRedirect(reverse("accounts:password_reset"))

    def get(self, request):
        form = PasswordResetConfirmForm()
        return render(request=request, template_name="accounts/password/reset.html", context={"form": form})

    def post(self, request):
        form = PasswordResetConfirmForm(request.POST)
        if form.is_valid():
            form.save(account=self.account)
            messages.add_message(request, messages.SUCCESS, _("You have successfully reset your password."))
            return HttpResponseRedirect(reverse("accounts:login"))
        return render(request=request, template_name="accounts/password/reset.html", context={"form": form})


class PasswordChangeView(AccessRestrictedMixin, AccessModelMixin, NextPageMixin, View):
    permissions = ("accounts.change_passwords",)
    personal = True
    model = Account

    def get(self, request):
        form = PasswordChangeForm()
        return render(request=request, template_name="accounts/password/change.html", context={"form": form})

    def post(self, request):
        form = PasswordChangeForm(request.POST)
        if form.is_valid():
            form.save(self.account)
            messages.add_message(request, messages.SUCCESS, _("You have successfully changed the password."))
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="accounts/password/change.html", context={"form": form})


class ListView(AccessRestrictedMixin, View):
    permissions = ("accounts.list_accounts", "accounts.edit_accounts", "accounts.delete_accounts")

    def get(self, request):
        accounts = Account.objects.all().order_by("last_name")
        return render(request=request, template_name="accounts/list.html", context={"accounts": accounts})


class DetailView(AccessRestrictedMixin, AccessModelMixin, View):
    permissions = ("accounts.view_accounts", "accounts.edit_accounts", "accounts.delete_accounts")
    personal = True
    model = Account

    def get(self, request):
        return render(request=request, template_name="accounts/detail.html", context={"account": self.account})


class EditView(AccessRestrictedMixin, AccessModelMixin, NextPageMixin, View):
    permissions = ("accounts.edit_accounts",)
    personal = True
    model = Account

    def get(self, request):
        form = EditForm(instance=self.account, initial={"verify_email": self.account.email})
        return render(request=request, template_name="accounts/edit.html", context={"form": form})

    def post(self, request):
        form = EditForm(request.POST, request.FILES, instance=self.account)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, _("The account has been successfully edited."))
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="accounts/edit.html", context={"form": form})


class DeleteView(AccessRestrictedMixin, AccessModelMixin, NextPageMixin, View):
    permissions = ("accounts.delete_accounts",)
    personal = True
    model = Account

    def get(self, request):
        return render(request=request, template_name="accounts/delete.html")

    def post(self, request):
        self.account.delete()
        messages.add_message(request, messages.SUCCESS, _("The account has been deleted."))
        return HttpResponseRedirect(self.next)


class EditPermissionsView(AccessRestrictedMixin, AccessModelMixin, NextPageMixin, View):
    permissions = "accounts.delete_accounts"
    model = Account

    def get(self, request):
        permissions = [perm.id for perm in Permission.objects.filter(user=self.account)]
        form = EditPermissionsForm(initial={"superuser": self.account.is_superuser, "permissions": permissions})
        return render(request=request, template_name="accounts/permissions/edit.html", context={"form": form})

    def post(self, request):
        form = EditPermissionsForm(request.POST)
        if form.is_valid():
            form.save(self.account)
            messages.add_message(request, messages.SUCCESS, _("You have successfully changed the permissions."))
            return HttpResponseRedirect(self.next)
        return render(request=request, template_name="accounts/permissions/edit.html", context={"form": form})
