import re
import datetime

from django import forms
from django.conf import settings
from django.contrib.auth import authenticate, login
from django.template.loader import render_to_string
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.tokens import default_token_generator as token_generator
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.html import strip_tags
from django.core.mail import send_mail
from django.contrib.auth.models import Permission

from tempus_dominus.widgets import DatePicker

from apps.accounts.models import Account


class LoginForm(forms.Form):
    email = forms.EmailField(label=_("Email"))
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput(render_value=False))

    def clean_email(self):
        return self.cleaned_data["email"].lower()

    def clean(self):
        if self._errors:
            return
        try:
            self.account = Account.objects.get(email=self.cleaned_data["email"])
            if not self.account.is_active:
                raise forms.ValidationError(_("Your account has not been activated."))
            self.account = authenticate(email=self.cleaned_data["email"], password=self.cleaned_data["password"])
            if self.account is None:
                raise Account.DoesNotExist
        except Account.DoesNotExist:
            raise forms.ValidationError(_("The email and/or password you entered are incorrect."))
        return self.cleaned_data

    def login(self, request):
        if self.is_valid():
            login(request, self.account)
            return True
        return False


class RegistrationForm(forms.ModelForm):
    verify_email = forms.EmailField(label=_("Verify Email"))
    password = forms.CharField(
        label=_("Password"),
        widget=forms.PasswordInput(render_value=False),
        help_text=_("Must have at least 8 characters, a letter, and a number"),
    )
    verify_password = forms.CharField(label=_("Verify Password"), widget=forms.PasswordInput(render_value=False))
    terms_of_service = forms.BooleanField(
        label=_("I agree to the Terms of Service"),
        error_messages={"required": "You must agree to the Terms of Service before registering."},
    )

    class Meta:
        model = Account
        widgets = {
            "date_of_birth": DatePicker(
                options={
                    "format": "YYYY-MM-DD",
                    "icons": {
                        "time": "fas fa-clock fa-fw",
                        "date": "fas fa-calendar fa-fw",
                        "up": "fas fa-arrow-up fa-fw",
                        "down": "fas fa-arrow-down fa-fw",
                    },
                },
                attrs={"append": "fas fa-calendar-alt fa-fw"},
            )
        }
        fields = (
            "email",
            "verify_email",
            "password",
            "verify_password",
            "title",
            "first_name",
            "last_name",
            "date_of_birth",
            "gender",
        )
        labels = {
            "first_name": "First Name",
            "last_name": "Last Name",
            "date_of_birth": "Date of Birth",
        }

    def clean_email(self):
        return self.cleaned_data["email"].lower()

    def clean_verify_email(self):
        return self.cleaned_data["verify_email"].lower()

    def clean_password(self):
        if not re.match(settings.PASSWORD_REGEX, self.cleaned_data["password"]):
            raise forms.ValidationError(_("The password needs to have at least 8 characters, a letter, and a number."))
        return self.cleaned_data["password"]

    def clean_first_name(self):
        if not re.match(settings.NAME_REGEX, self.cleaned_data["first_name"]):
            raise forms.ValidationError(_("Enter a valid first name."))
        return self.cleaned_data["first_name"]

    def clean_last_name(self):
        if not re.match(settings.NAME_REGEX, self.cleaned_data["last_name"]):
            raise forms.ValidationError(_("Enter a valid last name."))
        return self.cleaned_data["last_name"]

    def clean_date_of_birth(self):
        if self.cleaned_data["date_of_birth"] and (datetime.date.today() - self.cleaned_data["date_of_birth"]).days < 0:
            raise forms.ValidationError(_("The date of birth cannot be in the future."))
        return self.cleaned_data["date_of_birth"]

    def clean(self):
        self.cleaned_data = super().clean()
        if self._errors:
            return
        if self.cleaned_data["email"] != self.cleaned_data["verify_email"]:
            raise forms.ValidationError(_("The emails do not match."))
        if self.cleaned_data["password"] != self.cleaned_data["verify_password"]:
            raise forms.ValidationError(_("The passwords do not match."))
        return self.cleaned_data

    def save(self):
        account = super().save()
        account.set_password(self.cleaned_data["password"])
        account.save()
        return account


class PasswordResetForm(forms.Form):
    email = forms.EmailField(label=_("Email"))

    def clean_email(self):
        try:
            self.account = Account.objects.get(email=self.cleaned_data["email"].lower(), is_active=True)
        except Account.DoesNotExist:
            raise forms.ValidationError(_("The email is not associated with any active accounts."))
        return self.cleaned_data["email"].lower()

    def save(self, request):
        url = request.build_absolute_uri("/accounts/password/reset/")
        url += urlsafe_base64_encode(force_bytes(self.account.uid)) + "/"
        url += token_generator.make_token(self.account) + "/"

        html = render_to_string(
            "email.html", {"url": url, "message": _("You requested a password reset."), "button": "Reset Password"},
        )
        text = strip_tags(html).replace("Reset Password", url)

        send_mail(
            subject=_("Reset Password | %s") % settings.SITE_NAME,
            message=text,
            html_message=html,
            from_email=settings.EMAIL_HOST_USER,
            recipient_list=(self.cleaned_data["email"],),
        )


class PasswordResetConfirmForm(forms.Form):
    new_password = forms.CharField(label=_("New Password"), widget=forms.PasswordInput(render_value=False))
    verify_new_password = forms.CharField(
        label=_("Verify New Password"), widget=forms.PasswordInput(render_value=False)
    )

    def clean_new_password(self):
        if not re.match(settings.PASSWORD_REGEX, self.cleaned_data["new_password"]):
            raise forms.ValidationError(_("The password needs to have at least 8 characters, a letter, and a number."))
        return self.cleaned_data["new_password"]

    def clean(self):
        if self._errors:
            return
        if self.cleaned_data["new_password"] != self.cleaned_data["verify_new_password"]:
            raise forms.ValidationError(_("The passwords do not match."))
        return self.cleaned_data

    def save(self, account):
        account.set_password(self.cleaned_data["new_password"])
        account.save()
        return account


class PasswordChangeForm(forms.Form):
    new_password = forms.CharField(label=_("New Password"), widget=forms.PasswordInput(render_value=False))
    verify_new_password = forms.CharField(
        label=_("Verify New Password"), widget=forms.PasswordInput(render_value=False)
    )

    def clean_new_password(self):
        if not re.match(settings.PASSWORD_REGEX, self.cleaned_data["new_password"]):
            raise forms.ValidationError(_("The password needs to have at least 8 characters, a letter, and a number."))
        return self.cleaned_data["new_password"]

    def clean(self):
        if self._errors:
            return
        if self.cleaned_data["new_password"] != self.cleaned_data["verify_new_password"]:
            raise forms.ValidationError(_("The passwords do not match."))
        return self.cleaned_data

    def save(self, account):
        account.set_password(self.cleaned_data["new_password"])
        account.save()
        return account


class EditForm(forms.ModelForm):
    verify_email = forms.EmailField(label=_("Verify Email"))
    phone = forms.RegexField(
        label=_("Phone Number"),
        regex=r"^\+?[0-9]+$",
        max_length=16,
        required=False,
        help_text=_("Must have only digits and an optional country code"),
    )

    class Meta:
        model = Account
        widgets = {
            "date_of_birth": DatePicker(
                options={
                    "format": "YYYY-MM-DD",
                    "icons": {
                        "time": "fas fa-clock fa-fw",
                        "date": "fas fa-calendar fa-fw",
                        "up": "fas fa-arrow-up fa-fw",
                        "down": "fas fa-arrow-down fa-fw",
                    },
                },
                attrs={"append": "fas fa-calendar-alt fa-fw"},
            )
        }
        fields = (
            "email",
            "verify_email",
            "title",
            "first_name",
            "last_name",
            "date_of_birth",
            "gender",
            "company",
            "phone",
            "address1",
            "address2",
            "city",
            "state",
            "zipcode",
            "country",
            "avatar",
        )
        labels = {
            "first_name": "First Name",
            "last_name": "Last Name",
            "date_of_birth": "Date of Birth",
            "company": "Company / School / University",
            "address1": "Address Line 1",
            "address2": "Address Line 2",
            "state": "State / Region / Province",
            "zipcode": "ZIP / Postal Code",
        }

    def clean_email(self):
        return self.cleaned_data["email"].lower()

    def clean_verify_email(self):
        return self.cleaned_data["verify_email"].lower()

    def clean_first_name(self):
        if not re.match(settings.NAME_REGEX, self.cleaned_data["first_name"]):
            raise forms.ValidationError(_("Enter a valid first name."))
        return self.cleaned_data["first_name"]

    def clean_last_name(self):
        if not re.match(settings.NAME_REGEX, self.cleaned_data["last_name"]):
            raise forms.ValidationError(_("Enter a valid last name."))
        return self.cleaned_data["last_name"]

    def clean_date_of_birth(self):
        if self.cleaned_data["date_of_birth"] and (datetime.date.today() - self.cleaned_data["date_of_birth"]).days < 0:
            raise forms.ValidationError(_("The date of birth cannot be in the future."))
        return self.cleaned_data["date_of_birth"]

    def clean(self):
        self.cleaned_data = super().clean()
        if self._errors:
            return
        if self.cleaned_data["email"] != self.cleaned_data["verify_email"]:
            raise forms.ValidationError(_("The emails do not match."))
        return self.cleaned_data


class EditPermissionsForm(forms.Form):
    superuser = forms.BooleanField(label=_("Superuser"), required=False, help_text=_("Superusers have all permissions"))
    permissions = forms.MultipleChoiceField(label=_("Permissions"), required=False, widget=forms.CheckboxSelectMultiple)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["permissions"].choices = [
            (permission.id, permission.content_type.app_label.upper() + ": " + permission.name)
            for permission in Permission.objects.exclude(
                content_type__app_label__in=["auth", "sessions", "contenttypes"]
            )
        ]

    def save(self, account):
        account.is_superuser = self.cleaned_data["superuser"]
        if self.cleaned_data["permissions"]:
            account.user_permissions.set(self.cleaned_data["permissions"])
        else:
            account.user_permissions.clear()
        account.save()
