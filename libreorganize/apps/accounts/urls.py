from django.urls import path

from apps.accounts import views

app_name = "apps.accounts"

urlpatterns = [
    path("", views.ListView.as_view(), name="list"),
    path("login/", views.LoginView.as_view(), name="login"),
    path("logout/", views.LogoutView.as_view(), name="logout"),
    path("register/", views.RegisterView.as_view(), name="register"),
    path("password/reset/", views.PasswordResetView.as_view(), name="password_reset"),
    path("password/reset/<uidb64>/<token>/", views.PasswordResetConfirmView.as_view(), name="password_reset"),
    path("<int:uid>/password/change/", views.PasswordChangeView.as_view(), name="password_change"),
    path("<int:uid>/", views.DetailView.as_view(), name="detail"),
    path("<int:uid>/edit/", views.EditView.as_view(), name="edit"),
    path("<int:uid>/delete/", views.DeleteView.as_view(), name="delete"),
    path("<int:uid>/permissions/edit/", views.EditPermissionsView.as_view(), name="permissions_edit"),
]
