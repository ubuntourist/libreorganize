from django.db import models
from django.conf import settings
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.utils.translation import gettext_lazy as _
from django_countries.fields import CountryField

from apps.accounts.managers import AccountManager


class Account(AbstractBaseUser, PermissionsMixin):
    uid = models.AutoField(primary_key=True)
    email = models.EmailField(unique=True, error_messages={"unique": _("This email is already in use.")})
    title = models.CharField(max_length=8, choices=settings.TITLE_CHOICES, blank=True)
    first_name = models.CharField(max_length=32)
    last_name = models.CharField(max_length=32)

    date_of_birth = models.DateField(blank=True, null=True, help_text=_("Must be formatted as YYYY-MM-DD"))
    gender = models.CharField(choices=settings.GENDER_CHOICES, max_length=8, blank=True)
    company = models.CharField(max_length=64, blank=True)
    phone = models.CharField(max_length=16, blank=True, help_text=_("Must not have any spaces, paranthesis, or dashes"))
    address1 = models.CharField(max_length=96, blank=True, help_text=_("Street address, P.O. box"))
    address2 = models.CharField(max_length=64, blank=True, help_text=_("Apartment, suite, unit, building, floor"))
    city = models.CharField(max_length=32, blank=True)
    state = models.CharField(max_length=32, blank=True)
    zipcode = models.CharField(max_length=16, blank=True)
    country = CountryField(blank_label="", blank=True)

    avatar = models.ImageField(upload_to="img/avatars", blank=True, help_text=_("Use a 1:1 image for the best quality"))

    date_joined = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    objects = AccountManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        default_permissions = ()
        permissions = (
            ("list_accounts", "List accounts"),
            ("view_accounts", "View accounts"),
            ("edit_accounts", "Edit accounts"),
            ("delete_accounts", "Delete accounts"),
            ("change_passwords", "Change passwords"),
            ("access_dashboard", "Access dashboard"),
        )

    @property
    def avatar_url(self):
        if self.avatar:
            return self.avatar.url
        return settings.STATIC_URL + "img/base/profile_icon_default.png"

    def __str__(self):
        return self.get_full_name()

    def get_full_name(self):
        return self.first_name + " " + self.last_name

    def get_short_name(self):
        return self.first_name + " " + self.last_name[:1] + "."

    def get_full_address(self):
        full_address = ""
        if self.address1:
            full_address = self.address1
            if self.address2:
                full_address += ", "
                full_address += self.address2
        if self.city:
            if full_address != "":
                full_address += ", "
            full_address += self.city
        if self.state:
            if full_address != "":
                full_address += ", "
            full_address += self.state
        if self.zipcode:
            if full_address != "":
                full_address += " "
            full_address += self.zipcode
        if self.country:
            if full_address != "":
                full_address += ", "
            full_address += self.country.name
        return full_address
