from django.core.wsgi import get_wsgi_application
import env_file


env_file.load()
application = get_wsgi_application()
