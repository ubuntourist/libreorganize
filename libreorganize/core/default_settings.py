import os

from django.contrib.messages import constants as message_constants
from django.utils.translation import gettext_lazy as _

# ---------------------------------------------------------------------
# CORE
# ---------------------------------------------------------------------

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = os.environ["LO_SECRET_KEY"]

ALLOWED_HOSTS = ["*"]

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
NOTIFICATIONS_EMAIL = "admin@novawebdevelopment.org"
WSGI_APPLICATION = "core.wsgi.application"
ROOT_URLCONF = "core.default_urls"
AUTH_USER_MODEL = "accounts.Account"

SITE_NAME = "LibreOrganize"

# ---------------------------------------------------------------------
# DEBUGGING
# ---------------------------------------------------------------------

DEBUG = True

# ---------------------------------------------------------------------
# MISC
# ---------------------------------------------------------------------

NAME_REGEX = (
    r"^[A-Za-zàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð\.\,\' \-]+$"
)
PASSWORD_RESET_TIMEOUT_DAYS = 1
PASSWORD_REGEX = r"^.*(?=.{8,})(?=.*\d)(?=.*[a-zA-Z]).*$"

# ---------------------------------------------------------------------
# CRISPY FORMS
# ---------------------------------------------------------------------

CRISPY_TEMPLATE_PACK = "bootstrap4"
MESSAGE_TAGS = {
    message_constants.DEBUG: "debug",
    message_constants.INFO: "info",
    message_constants.SUCCESS: "success",
    message_constants.WARNING: "warning",
    message_constants.ERROR: "danger",
}

# ---------------------------------------------------------------------
# EMAIL
# ---------------------------------------------------------------------

EMAIL_HOST = os.environ["LO_EMAIL_HOST"]
EMAIL_USE_TLS = True
EMAIL_PORT = 587
EMAIL_HOST_USER = os.environ["LO_EMAIL_HOST_USER"]
EMAIL_HOST_PASSWORD = os.environ["LO_EMAIL_HOST_PASSWORD"]

# ---------------------------------------------------------------------
# STATIC AND MEDIA FILES
# ---------------------------------------------------------------------

CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_CONFIGS = {
    "default": {
        "toolbar": "LibreOrganize",
        "toolbar_LibreOrganize": [
            ["Bold", "Italic", "Underline", "Strike", "Subscript", "Superscript"],
            ["JustifyLeft", "JustifyCenter", "JustifyRight"],
            ["FontSize"],
            ["HorizontalRule"],
            ["Link", "Unlink"],
            ["Image", "Iframe", "Templates", "CodeSnippet"],
            ["Source"],
        ],
        "height": 256,
        "width": "100%",
        "resize_minHeight": 256,
        "resize_maxHeight": 512,
        "basicEntities": False,
        "entities": False,
        "extraPlugins": "codesnippet",
        "removePlugins": "elementspath",
    },
}

# ---------------------------------------------------------------------
# PACKAGES
# ---------------------------------------------------------------------

INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django_countries",
    "crispy_forms",
    "ckeditor",
    "ckeditor_uploader",
    "tempus_dominus",
    "core",
    "apps.accounts",
    "apps.boxes",
    "apps.events",
    "apps.memberships",
    "apps.wikis",
    "django_cleanup.apps.CleanupConfig",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

# ---------------------------------------------------------------------
# VALIDATORS
# ---------------------------------------------------------------------

AUTH_PASSWORD_VALIDATORS = [
    {"NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",},
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator", "OPTIONS": {"min_length": 8,},},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",},
]

# ---------------------------------------------------------------------
# TEMPLATES
# ---------------------------------------------------------------------

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

# ---------------------------------------------------------------------
# DATABASE
# ---------------------------------------------------------------------

if DEBUG:
    DATABASES = {"default": {"ENGINE": "django.db.backends.sqlite3", "NAME": "libreorganize.sqlite3"}}
else:
    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.postgresql_psycopg2",
            "NAME": os.environ["LO_DB_NAME"],
            "USER": os.environ["LO_DB_USER"],
            "PASSWORD": os.environ["LO_DB_PASSWORD"],
            "HOST": "localhost",
            "PORT": "",
        }
    }

# ---------------------------------------------------------------------
# INTERNATIONALIZATION
# ---------------------------------------------------------------------

LANGUAGE_CODE = "en"
TIME_ZONE = "UTC"

USE_I18N = True
USE_L10N = True
USE_TZ = False

# ---------------------------------------------------------------------
# STATIC AND MEDIA FILES
# ---------------------------------------------------------------------

STATIC_URL = "/static/"
MEDIA_URL = "/media/"

LOCALE_PATHS = (os.path.join(BASE_DIR, "locale"),)

if DEBUG:
    MEDIA_ROOT = os.path.join(BASE_DIR, "media")
else:
    STATIC_ROOT = "/var/www/libreorganize/static/"
    MEDIA_ROOT = "/var/www/libreorganize/media/"

# ---------------------------------------------------------------------
#  CHOICES
# ---------------------------------------------------------------------

TITLE_CHOICES = (
    ("", ""),
    ("mr", _("Mr.")),
    ("mx", _("Mx.")),
    ("mrs", _("Mrs.")),
    ("ms", _("Ms.")),
    ("miss", _("Miss")),
    ("dr", _("Dr.")),
    ("prof", _("Prof.")),
)

GENDER_CHOICES = (
    ("", ""),
    ("male", _("Male")),
    ("female", _("Female")),
    ("other", _("Other")),
)

LANGUAGES = (("en", _("English")), ("es", _("Spanish")))

MEMBERSHIP_TYPES = (("student", _("Student - $5.00")), ("normal", _("Normal - $10.00")))

PAYMENT_METHODS = (("cash", _("Cash")),)
