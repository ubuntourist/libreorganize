from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView
from django.urls import path, include

from ckeditor_uploader.views import upload


urlpatterns = [
    path("i18n/", include("django.conf.urls.i18n")),
    path("accounts/", include("apps.accounts.urls", namespace="accounts")),
    path("boxes/", include("apps.boxes.urls", namespace="boxes")),
    path("events/", include("apps.events.urls", namespace="events")),
    path("memberships/", include("apps.memberships.urls", namespace="memberships")),
    path("wikis/", include("apps.wikis.urls", namespace="wikis")),
    path("ckeditor/upload/", upload, name="ckeditor_upload"),
    path("ckeditor/", include("ckeditor_uploader.urls")),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
